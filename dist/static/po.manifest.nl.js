window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "nl",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "Diagnostische rapporten"
 ],
 "Kernel dump": [
  null,
  "Kerneldump"
 ],
 "Networking": [
  null,
  "Netwerken"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "Opslag"
 ]
};
