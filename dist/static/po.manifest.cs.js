window.cockpit_po = {
 "": {
  "plural-forms": (n) => (n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2,
  "language": "cs",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "Diagnostická hlášení"
 ],
 "Kernel dump": [
  null,
  "Výpis paměti jádra"
 ],
 "Networking": [
  null,
  "Síť"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "Úložiště"
 ]
};
